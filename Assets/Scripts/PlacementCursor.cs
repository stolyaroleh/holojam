﻿using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class PlacementCursor : Singleton<PlacementCursor>
{
    public float BaseDistance = 1f;

    public Vector3 targetPosition;

    public void RepositionCursor()
    {
        targetPosition = Camera.main.inFrontOf(BaseDistance);
        if (HandsManager.Instance.DetectedHands.Count > 0)
        {
            targetPosition = (HandsManager.Instance.DetectedHands[0].Position) + Camera.main.inFrontOf(BaseDistance) - Camera.main.transform.position;
        }

        CursorManager.Instance.transform.position = targetPosition;
    }

    public void Place(InteractionSourceKind source, int tapCount, Ray headRay)
    {
        if (States.Current != States.State.Placing)
        {
            return;
        }
        Score.Instance.CreateNoteFromWorldPosition(targetPosition);
    }
}
