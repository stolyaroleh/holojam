﻿using UnityEngine;

public class OrientToCamera : MonoBehaviour
{
    public RectTransform rectTransform;

	void Update ()
	{
	    rectTransform.position = transform.position;
	    transform.forward = Camera.main.transform.forward;
	}
}
