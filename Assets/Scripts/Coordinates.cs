﻿using UnityEngine;

public static class Coordinates {
    public static Vector3 fromCylindrical(float r, float z, float theta)
    {
        return new Vector3(r * Mathf.Cos(theta),
                           z,
                           r * Mathf.Sin(theta));
    }

}
