﻿using UnityEngine;
using System.Collections;

public class ClearOnSelect : MonoBehaviour {

    void OnSelect()
    {
        Score.Instance.ClearNotes();
    }
}
