﻿using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.UI;

public class NoteMenuManager : Singleton<NoteMenuManager>
{
    public Canvas canvas;
    private GameObject target;

    public void ShowBehind(GameObject o)
    {
        if (target != null)
        {
            return;
        }
        target = o;
        canvas.gameObject.SetActive(true);
    }

    public void Hide()
    {
        target = null;
        canvas.gameObject.SetActive(false);
    }

	void Update () {
	    if (target == null)
	    {
	        return;
	    }

	    var targetPos = target.transform.position + Camera.main.transform.forward;
	    canvas.transform.position = targetPos;
	    canvas.transform.forward = Camera.main.transform.forward;
	}
}
