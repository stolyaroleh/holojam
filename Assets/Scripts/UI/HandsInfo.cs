﻿using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.UI;

public class HandsInfo : MonoBehaviour
{
    public Text Label;

    void Update()
    {
        HandsManager.Hand left = HandsManager.Instance.Left();
        HandsManager.Hand right = HandsManager.Instance.Right();

        Label.text = string.Format("Hands\n\n" +
                                   "Left: \t {0}\n" +
                                   "Pressed: \t{1}\n" +
                                   "Position: \t {2}\n" +
                                   "Velocity: \t {3}\n" +
                                   "Right: \t {4}\n" +
                                   "Pressed: \t{5}\n" +
                                   "Position: \t {6}\n" +
                                   "Velocity: \t {7}\n",
            left.Detected,
            left.Pressed,
            left.Position,
            left.Velocity,
            right.Detected,
            right.Pressed,
            right.Position,
            right.Velocity);
    }
}