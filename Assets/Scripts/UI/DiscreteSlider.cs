﻿using UnityEngine;

public class DiscreteSlider : MonoBehaviour {
    public struct Axis
    {
        public string Name;
        public string[] ValueNames;

        public float Snap(float position)
        {
            int length = ValueNames.Length;
            return Mathf.Floor((length - 1)*position + 0.5f);
        }

        public int SelectedIndex(float position)
        {
            return Mathf.FloorToInt(position);
        }
    }
}
