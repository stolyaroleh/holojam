﻿using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class Instrument : MonoBehaviour
{
    public Material SelectionMaterial;

    public MeshRenderer meshRenderer;
    private Material originalMaterial;
    private bool selected;

    void Start()
    {
        originalMaterial = meshRenderer.material;
    }

    void OnSelect()
    {
        selected = !selected;
        meshRenderer.material = selected ? SelectionMaterial : originalMaterial;
        if (selected)
        {
            NoteMenuManager.Instance.ShowBehind(gameObject);
        }
        else
        {
            NoteMenuManager.Instance.Hide();
        }
    }
}
