﻿using System;

public class States {
    public enum State
    {
        Placing,
        LookingAt,
        Selected
    }

    public static State Current;
}