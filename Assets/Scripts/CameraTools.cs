﻿using UnityEngine;
using System.Collections;

public static class CameraTools {
    public static Vector3 inFrontOf(this Camera camera, float distance)
    {
        return camera.transform.position + camera.transform.forward*distance;
    }
}
