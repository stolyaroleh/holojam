﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using HoloToolkit.Unity;

public class Score : Singleton<Score>
{
    public GameObject noteToPlace;

    public float ZSize;
    public float YSize;

    public int NumTracks = 8;
    public int NumRings = 8;
    public int BeatsPerTrack = 64;

    public float DivisionSize;
    Note[,,] _score;
    List<GameObject> noteModels;

    void Start()
    {
        _score = new Note[NumTracks, BeatsPerTrack, NumRings];
        noteModels = new List<GameObject>();
        int total = NumTracks*BeatsPerTrack*NumRings;
        Vector3[] positions = new Vector3[total];
        Color[] colors = new Color[total];
        int[] indices = new int[total];
        int i = 0;
        for (int track = 0; track < NumTracks; track++)
        {
            for (int ring = 0; ring < NumRings; ring++)
            {
                for (int beat = 0; beat < BeatsPerTrack; beat++)
                {
                    positions[i] = GetPosition(track, beat, ring);
                    colors[i] = Color.cyan;
                    indices[i] = i;
                    i++;
                }
            }
        }
        Mesh mesh = new Mesh()
        {
            vertices = positions,
            colors = colors
        };
        mesh.SetIndices(indices, MeshTopology.Points, 0);
        var meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;
    }

    public List<Note> GetNotesForBeat(int inBeat)
    {
        var outNotes = new List<Note>();
        for (int track = 0; track < NumTracks; track++)
        {
            for (int ring = 0; ring < NumRings; ring++)
            {
                if (_score[track, inBeat, ring])
                    outNotes.Add(_score[track, inBeat, ring]);
            }
        }
        return outNotes;
    }

    public void MoveNote(int OldTrack, int OldBeat, int OldRing, int NewTrack, int NewBeat, int NewRing)
    {
        _score[NewTrack, NewBeat, NewRing] = _score[OldTrack, OldBeat, OldRing];
        _score[OldTrack, OldBeat, OldRing] = null;
        _score[NewTrack, NewBeat, NewRing].ring = NewRing;
        _score[NewTrack, NewBeat, NewRing].beat = NewBeat;
        _score[NewTrack, NewBeat, NewRing].track = NewTrack;
        SnapToPosition(NewTrack, NewBeat, NewRing);
    }

    void SnapToPosition(int track, int beat, int ring)
    {
        Vector3 pos = transform.TransformPoint(GetPosition(track, beat, ring));
        _score[track, beat, ring].transform.position = pos;
    }

    public void CreateNoteFromWorldPosition(Vector3 worldPos)
    {
        int minTrack, minRing, minBeat;
        GetClosestCoords(worldPos, out minTrack, out minBeat, out minRing);
        CreateNote(minTrack, minBeat, minRing);
    }

    public void CreateNote(int inTrack, int inBeat, int inRing)
    {
        var note = Instantiate(noteToPlace).GetComponent<Note>();
        _score[inTrack, inBeat, inRing] = note;
        _score[inTrack, inBeat, inRing].Init(inTrack, inBeat, inRing);
        SnapToPosition(inTrack, inBeat, inRing);
        noteModels.Add(note.gameObject);
    }

    public void ClearNotes()
    {
        _score = new Note[NumTracks, BeatsPerTrack, NumRings];
        foreach (var note in noteModels) {
            Destroy(note);
        }
        noteModels = new List<GameObject>();
    }

    Vector3 GetPosition(int track, int beat, int ring)
    {
        float dThetaRad = Mathf.PI*2/BeatsPerTrack;
        float z = (track + 0.5f)*YSize;
        float r = (ring + 0.5f)*ZSize;
        float theta = beat*dThetaRad;
        var position = Coordinates.fromCylindrical(r, z, theta);
        return position;
    }

    private void GetClosestCoords(Vector3 worldPos, out int minTrack, out int minBeat, out int minRing)
    {
        minTrack = minBeat = minRing = 0;
        float mindstsqr = float.MaxValue;
        for (int track = 0; track < NumTracks; track++)
        {
            for (int beat = 0; beat < BeatsPerTrack; beat++)
            {
                for (int ring = 0; ring < NumRings; ring++)
                {
                    var dst =
                        Vector3.SqrMagnitude(transform.TransformPoint(GetPosition(track, beat, ring)) - worldPos);
                    if (dst < mindstsqr)
                    {
                        mindstsqr = dst;
                        minTrack = track;
                        minBeat = beat;
                        minRing = ring;
                    }
                }
            }
        }
    }

    public Vector3 GetClosestPoint(Vector3 worldPos)
    {
        int minTrack, minBeat, minRing;
        GetClosestCoords(worldPos, out minTrack, out minBeat, out minRing);
        return transform.TransformPoint(GetPosition(minTrack, minBeat, minRing));
    }
}