﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

namespace HoloToolkit.Unity
{
    public partial class HandsManager
    {
        public struct Hand
        {
            public bool Detected;
            public bool Pressed;
            public Vector3 Position;
            public Vector3 Velocity;
        }

        public List<Hand> DetectedHands = new List<Hand>();

        public Hand Left()
        {
            if (DetectedHands.Count < 2)
            {
                return new Hand
                {
                    Detected = false,
                    Pressed = false,
                    Position = Vector3.zero,
                    Velocity = Vector3.zero
                };
            }
            Hand first = DetectedHands[0];
            Hand second = DetectedHands[1];
            float firstPos = Camera.main.WorldToScreenPoint(first.Position).x;
            float secondPos = Camera.main.WorldToScreenPoint(second.Position).x;
            return firstPos > secondPos ? second : first;
        }

        public Hand Right()
        {
            if (DetectedHands.Count == 0)
            {
                return new Hand
                {
                    Detected = false,
                    Pressed = false,
                    Position = Vector3.zero,
                    Velocity = Vector3.zero
                };
            }
            if (DetectedHands.Count == 1)
            {
                return DetectedHands[0];
            }
            Hand first = DetectedHands[0];
            Hand second = DetectedHands[1];
            float firstPos = Camera.main.WorldToScreenPoint(first.Position).x;
            float secondPos = Camera.main.WorldToScreenPoint(second.Position).x;
            return firstPos > secondPos ? first : second;
        }

        void Update()
        {
            DetectedHands.Clear();
            var currentReading = InteractionManager.GetCurrentReading();
            for (int i = 0; i < trackedHands.Count; i++)
            {
                uint id = trackedHands[i];
                for (int j = 0; j < currentReading.Length; j++)
                {
                    var state = currentReading[j];
                    if (state.source.id != id)
                    {
                        continue;
                    }

                    Vector3 position;
                    Vector3 velocity;
                    state.properties.location.TryGetPosition(out position);
                    state.properties.location.TryGetVelocity(out velocity);

                    DetectedHands.Add(new Hand
                    {
                        Detected = true,
                        Pressed = state.pressed,
                        Position = position,
                        Velocity = velocity
                    });
                }
            }
        }
    }
}