﻿using UnityEngine;
using System.Collections;
using Random = System.Random;
using Math = System.Math;

public class Note : MonoBehaviour
{
    public string soundName;

    AudioSource mAudioSource;
    ParticleSystem mParticleSystem;
    Rigidbody mRigidbody;

    public Material SelectionMaterial;

    private MeshRenderer meshRenderer;
    private Material originalMaterial;
    private bool selected;

    public int track, beat, ring, soundIndex;

    public SampleLibrary.Sound sound;
    static Random random = new Random();

    private bool pulse = false;

    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        originalMaterial = meshRenderer.material;
    }

    void OnSelect()
    {
        selected = !selected;
        meshRenderer.material = selected ? SelectionMaterial : originalMaterial;
        if (selected)
        {
            NoteMenuManager.Instance.ShowBehind(gameObject);
        }
        else
        {
            NoteMenuManager.Instance.Hide();
        }
    }

    public void Init(int track_, int beat_, int ring_)
    {
        mParticleSystem = GetComponent<ParticleSystem>();
        mAudioSource = GetComponent<AudioSource>();
        mRigidbody = GetComponent<Rigidbody>();

        sound = SampleLibrary.Instance.GetRandomSound();

        if (random.NextDouble() < 0.5)
        {
            mAudioSource.clip = Resources.Load("Sounds/clankslow") as AudioClip;
        }
        else
        {
            mAudioSource.clip = Resources.Load("Sounds/" + sound.File) as AudioClip;
        }
        Normalize(mAudioSource.clip);

//        GameObject instrument = Resources.Load("Models/" + sound.Model) as GameObject;
        GameObject instrument = Resources.Load("Models/Lightning Blue") as GameObject;
        var obj = Instantiate(instrument);
        obj.transform.parent = transform;

        meshRenderer = GetComponentInChildren<MeshRenderer>();
        originalMaterial = meshRenderer.material;
        track = track_;
        beat = beat_;
        ring = ring_;
        soundIndex = random.Next(0, 14);
    }

    public void SetColor(Color inColor)
    {
        GetComponent<Renderer>().material.color = inColor;
        if (mParticleSystem)
            mParticleSystem.startColor = inColor;
    }

    public void SchedulePlayback(float inSeconds)
    {
        int pitchLevel = Math.Min(sound.PitchLevels.Length - 1, ring);
        mAudioSource.pitch = sound.PitchLevels[pitchLevel];
        mAudioSource.PlayScheduled(AudioSettings.dspTime + inSeconds);
        StartCoroutine(Pulse());
    }

    void Update()
    {
        if (pulse)
        {
            StartCoroutine(Pulse());
        }
    }

    // Increase volume so the peak is at +- 1.0
    private void Normalize(AudioClip clip)
    {
        float[] samples = new float[clip.samples * clip.channels];
        clip.GetData(samples, 0);
        int i = 0;
        float max = 0.0F;
        while (i < samples.Length) {
          max = Math.Max(max, Math.Abs(samples[i]));
          ++i;
        }
        float factor = 1.0F / max;
        i = 0;
        while (i < samples.Length) {
            samples[i] = samples[i] * factor;
            ++i;
        }
        clip.SetData(samples, 0);
    }

    private IEnumerator Pulse()
    {
        pulse = false;
        transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        float t = 0;
        float end = Mathf.PI/2;
        float step = end/10;
        while (t < end)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, Mathf.Sin(t));
            t += step;
            yield return new WaitForSeconds(0.1f);
        }
    }
}