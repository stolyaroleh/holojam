﻿using System;
using UnityEngine;
using HoloToolkit.Unity;
using SimpleJSON;
using Random = UnityEngine.Random;

public class SampleLibrary : Singleton<SampleLibrary>
{
    public TextAsset database;

    [Serializable]
    public struct Sound
    {
        public string Name;

        // Resources/Sounds
        public string File;
        // Resources/Models
        public string Model;
        // Resources/Icons
        public string Icon;

        public float[] PitchLevels;
    }

    public Sound[] Sounds;

    void Start()
    {
        LoadSounds();
    }

    string[] GetSoundNames()
    {
        string[] result = new string[Sounds.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = Sounds[i].Name;
        }
        return result;
    }

    public Sound GetRandomSound()
    {
        return Sounds[Random.Range(0, Sounds.Length)];
    }

    public Sound FindByName(string name)
    {
        foreach (var sound in Sounds)
        {
            if (sound.Name == name)
            {
                return sound;
            }
        }
        return Sounds[0];
    }

    void LoadSounds()
    {
        var json = JSON.Parse(database.text);

        Sounds = new Sound[json.Count];
        for (int i = 0; i < json.Count; i++)
        {
            string name = json[i]["name"].Value;
            string file = json[i]["filename"].Value;
            string mesh = json[i]["mesh"].Value;
            string image = json[i]["image"].Value;
            var pitches = json[i]["pitches"];
            float[] pitchLevels = new float[pitches.Count];
            for (int j = 0; j < pitches.Count; j++)
            {
                pitchLevels[j] = pitches[j].AsFloat;
            }
            Sounds[i] = new Sound
            {
                Name = name,
                File = file,
                Model = mesh,
                Icon = image,
                PitchLevels = pitchLevels
            };
        }
    }
}
