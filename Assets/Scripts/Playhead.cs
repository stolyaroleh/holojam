﻿using HoloToolkit.Unity;
using UnityEngine;

public class Playhead : Singleton<Playhead>
{
    // [0, 1)
    [Range(0.5f,10)]
    public float Period;
    public float CurrentPosition;
    public float CurrentStartedOn;

    public MeshFilter meshFilter;

    private float previousTime;
    private Score Score;
    private int lastBeat = -1;

    // Dimensions of playhead plane, and the number of points rendered in each dimension
    public float planeHeight = 0.5F;
    public int planeHeightPoints = 64;
    public float planeWidth = 0.5F;
    public int planeWidthPoints = 64;
    public Vector3[] planePoints;
    public Color[] planePointsColors;
    public int[] planePointsIndexes;

    void Start()
    {
        Score = FindObjectOfType<Score>();
        CurrentStartedOn = previousTime = Time.realtimeSinceStartup;
        GeneratePlanePoints();
        Mesh mesh = new Mesh
        {
            vertices = planePoints,
            colors = planePointsColors
        };
        mesh.SetIndices(planePointsIndexes, MeshTopology.Points, 0);
        meshFilter.mesh = mesh;
    }

    void LateUpdate()
    {
        float sinceStartup = Time.realtimeSinceStartup;
        float timePassed = Time.realtimeSinceStartup - previousTime;
        previousTime = sinceStartup;

   
        float distancePerBeat = 1.0f / Score.BeatsPerTrack;
        float nextPosition = CurrentPosition - timePassed / Period;
        for (int beat = 0; beat < Score.BeatsPerTrack; beat++)
        {
            if (nextPosition < beat * distancePerBeat && CurrentPosition >= beat * distancePerBeat)
            {
                foreach(var note in Score.GetNotesForBeat(beat))
                {
                    note.SchedulePlayback(0);
                }
            }

        }

        CurrentPosition += timePassed / Period;
        if (CurrentPosition >= 1.0f)
        {
            CurrentPosition -= Mathf.Floor(CurrentPosition);
            CurrentStartedOn = sinceStartup;
        }

        UpdateRotation();
    }

    void UpdateRotation()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0f, -360 * CurrentPosition, 0f));
    }

    void GeneratePlanePoints()
    {
        int numPoints = planeHeightPoints * planeWidthPoints;
        planePoints = new Vector3[numPoints];
        planePointsColors = new Color[numPoints];
        planePointsIndexes = new int[numPoints];
        for (int y = 0; y < planeHeightPoints; y++)
        {
            for (int x = 0; x < planeWidthPoints; x++)
            {
                int index = y * planeWidthPoints + x;
                planePoints[index] = new Vector3(x * planeWidth / planeWidthPoints, y * planeHeight / planeHeightPoints);
                planePointsColors[index] = new Color(1.0F, 0.9F, 0.9F, 1.0F);
                planePointsIndexes[index] = index;
            }
        }

    }
}
